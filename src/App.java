import models.Person;
import models.Staff;
import models.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Truong Tan Loc", "nguyen van linh");
        System.out.println("Person 1: ");
        System.out.println(person1);
        
        System.out.println("---------------------------------------");

        Person person2 = new Person("Vuong Thi My Cam", "nguyen van linh");
        System.out.println("Person 2: ");
        System.out.println(person2);
        
        System.out.println("---------------------------------------");

        Student student1 = new Student("Truong Tan Loc","Nguyen Van Linh", "Window", 2003, 2.5);
        System.out.println("Student 1: ");
        System.out.println(student1);
        System.out.println("---------------------------------------");

        Student student2 = new Student("Vuong Thi My Cam","Nguyen Van Linh", "Window", 2002, 4.5);
        System.out.println("Student 2: ");
        System.out.println(student2);
        System.out.println("---------------------------------------");

        Staff staff1 = new Staff("Truong Tan Loc", "Nguyen Van Linh", "Fpt university", 10000000);
        System.out.println("Staff 1: ");
        System.out.println(staff1);
        System.out.println("---------------------------------------");

        Staff staff2 = new Staff("vuong Thi My Cam", "Nguyen Van Linh", "Fpt university", 30000000);
        System.out.println("Staff 2: ");
        System.out.println(staff2);
}
}
